import numpy as  np

#pythran export iterates(float64, float64, int64)
def iterates(cx, cy, N):
    u,v,k = 0., 0., 0
    while (np.sqrt(u*u+v*v)<2. and k<N):
        t = u*u-v*v+cx
        v = 2*u*v+cy
        u = t
        k += 1
    return k
