import numpy as np
from matplotlib import pyplot
import time

from projet_3_pythran import iterates

time_start = time.time()
# Image parameters
N = 100  # Max. iterations
nx = 200  # width
ny = 200  # height
Ox, Oy = -1.5, -1  # Origin
Lx, Ly = 2., 2.  # Length
filename = "output_pythran.png"

x, y = np.meshgrid(np.linspace(Ox, Ox+Lx, nx),
                   np.linspace(Oy, Oy+Ly, ny), sparse=True)
A=np.zeros((ny,nx), dtype=np.int64)

A[...] = np.vectorize(iterates)(x,y,N)

pyplot.clf()
pyplot.imshow(A, extent=(Ox,Ox+Lx, Oy, Oy+Ly))
pyplot.colorbar()
#pyplot.show()
pyplot.savefig(filename)
time_end = time.time() - time_start
print(f"time for 1 proc : {time_end}")

