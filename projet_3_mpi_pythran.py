import numpy as np
from matplotlib import pyplot
import mpi4py.MPI as MPI
from projet_3_pythran import iterates

comm = MPI.COMM_WORLD
size = comm.size
rank = comm.rank

if rank == 0:
    time_start = MPI.Wtime()
# Image parameters
N = 100  # Max. iterations
nx = 200  # width
ny = 200  # height
Ox, Oy = -1.5, -1  # Origin
Lx, Ly = 2., 2.  # Length
filename = "output_mpi_pythan.png"

if(size > 1):
    range_size = (ny//(size-1))+1
    range_start = (rank) * range_size
    range_end = (rank + 1) * range_size
    y_range_size = (range_size*Ly)/ny
    y_range_start = Oy + (rank * y_range_size)
    y_range_end =  Oy + ((rank+1) * y_range_size)
    if(range_end > ny):
        range_end = ny
    if(y_range_end > Oy+Ly):
        y_range_end = Oy+Ly
else:
   range_size = ny
   range_start = 0
   range_end = ny
   y_range_size = ny
   y_range_start = Oy
   y_range_end = Oy + Ly

x, y = np.meshgrid(np.linspace(Ox, Ox+Lx, nx),
                   np.linspace(y_range_start, y_range_end, range_size), sparse=True)
                   
B = np.zeros((range_size, nx), dtype=np.int64)



B[...] = np.vectorize(iterates)(x,y,N)



to_send = (B.flatten(), range_size*nx, MPI.LONG)

to_recv = (np.empty((size, nx*range_size), dtype=np.int64), range_size*nx, MPI.LONG)
comm.Gatherv(to_send, to_recv, root = 0)
if(rank == 0):
    output_flatten = to_recv[0].flatten()
    output = output_flatten[:ny*nx].reshape((ny,nx))
    pyplot.clf()
    pyplot.imshow(output, extent=(Ox,Ox+Lx, Oy, Oy+Ly))
    pyplot.colorbar()
    #pyplot.show()
    pyplot.savefig(filename)
    time_end = MPI.Wtime() - time_start
    print(f"time for {size} proc : {time_end}")

